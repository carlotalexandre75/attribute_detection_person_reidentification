import json
import cv2
import os
import numpy as np
import pandas as pd
from typing import Tuple
from keras import Input, Model
from keras.metrics import sparse_categorical_accuracy, binary_accuracy, categorical_accuracy
from keras.applications.resnet50 import ResNet50, preprocess_input
from keras.layers import Dense, Lambda, MaxPooling2D, Flatten, Concatenate, BatchNormalization, Dropout,Multiply
from IPython.display import Image
import os
from subprocess import call


def pre_process_y():
    with open('market_attribute.json') as file:
        data = json.load(file)

    train = data['train']
    test = data['test']

    df_train = pd.DataFrame(
        data=list(zip(train['age'], train['backpack'], train['bag'], train['handbag'], train['downblack'], train['downblue'],
                train['downbrown'], train['downgray'], train['downgreen'], train['downpink'], train['downpurple'],
                train['downwhite'], train['downyellow'], train['upblack'], train['upblue'], train['upgreen'], train['upgray'],
                train['uppurple'], train['upred'], train['upwhite'], train['upyellow'], train['clothes'], train['down'],
                train['up'], train['hair'], train['hat'], train['gender'], train['image_index'])),
        columns=train.keys())

    df_test = pd.DataFrame(
        data=list(zip(test['age'], test['backpack'], test['bag'], test['handbag'], test['downblack'], test['downblue'],
                test['downbrown'], test['downgray'], test['downgreen'], test['downpink'], test['downpurple'],
                test['downwhite'], test['downyellow'], test['upblack'], test['upblue'], test['upgreen'], test['upgray'],
                test['uppurple'], test['upred'], test['upwhite'], test['upyellow'], test['clothes'], test['down'],
                test['up'], test['hair'], test['hat'], test['gender'], test['image_index'])),
        columns=test.keys())

    df_train.age = df_train.age < 2
    df_train.age = df_train.age.astype(int)
    df_train[['backpack', 'bag', 'handbag', 'downblack', 'downblue',
        'downbrown', 'downgray', 'downgreen', 'downpink', 'downpurple',
        'downwhite', 'downyellow', 'upblack', 'upblue', 'upgreen', 'upgray',
        'uppurple', 'upred', 'upwhite', 'upyellow', 'clothes', 'down', 'up',
        'hair', 'hat', 'gender']] -= 1

    df_test.age = df_test.age < 2
    df_test.age = df_test.age.astype(int)
    df_test[['backpack', 'bag', 'handbag', 'downblack', 'downblue',
        'downbrown', 'downgray', 'downgreen', 'downpink', 'downpurple',
        'downwhite', 'downyellow', 'upblack', 'upblue', 'upgreen', 'upgray',
        'uppurple', 'upred', 'upwhite', 'upyellow', 'clothes', 'down', 'up',
        'hair', 'hat', 'gender']] -= 1

    print('---- PREPROCESS ----')
    print('DF TRAIN')
    print(df_test.shape)
    print('TRAIN IS NA ?')
    print(df_train.isna().any())
    print('TRAIN IS NULL ?')
    print(df_train.isnull().any())
    print('DF TEST')
    print(df_test.shape)
    print('TEST IS NA ?')
    print(df_test.isna().any())
    print('TEST IS NULL ?')
    print(df_test.isnull().any())
    # df_train = df_train.drop(['image_index'], axis=1)
    return df_train, df_test, train['image_index']

def REID_models(n_ids: int=751, n_attributes: int=27):
    input = Input(shape=(128, 64, 3))

    Resnet = ResNet50(input_shape=(128, 64, 3), pooling="avg", weights='imagenet', include_top=False)(input)
    # goobal average pooling, trained on imagenet, without last layer
    layer_attr = Dense(n_attributes, activation="sigmoid", name="attributes")(Resnet)
    # RE WEIGHTING
    weight_attr = Dense(n_attributes, activation="sigmoid")(layer_attr)
    reweighted_attribute = Multiply()([layer_attr, weight_attr])

    id_layer = Dense(512, activation="relu")(Resnet)
    id_layer = BatchNormalization()(id_layer)
    id_layer = Dropout(rate=0.5)(id_layer)
    id_layer = Concatenate(axis=-1, name="image_features")([reweighted_attribute, id_layer])
    training_id_layer = Dense(n_ids, activation="softmax", name="ids")(id_layer)

    model_attr = Model(input, layer_attr)
    model_id = Model(input, id_layer)
    model_id_attr = Model(input, [layer_attr, training_id_layer])

    # SHOW MODELS
    print('---- GET MODELS ----')
    print('ATTRIBUTE MODEL')
    model_attr.summary
    print('ATTRIBUTE + FEATURES (PAPER) MODEL')
    model_id.summary()
    print('ID MODEL')
    model_id_attr.summary()

    return (model_attr, model_id, model_id_attr)

def load_imgs(df_test, df_train):
    image_train_files = os.listdir('train')
    image_test_files = os.listdir('test')
    images = []
    images_train = []
    images_test = []
    files_test, files_train = [], []
    files_test_name, files_train_name = [], []

    for path in image_train_files:
        img = cv2.imread(path)
        imgpath = os.path.join('train', path)
        img = cv2.imread(imgpath)
        files_train.append(imgpath)
        files_train_name.append(path[0:4])
        images_train.append(img)
    for path in image_test_files:
        img = cv2.imread(path)
        imgpath = os.path.join('test', path)
        img = cv2.imread(imgpath)
        files_test.append(imgpath)
        files_test_name.append(path[0:4])
        images_test.append(img)

    df_train_with_imgs = pd.DataFrame(data=list(zip(images_train, files_train, files_train_name)),
                                    columns=['img', 'path2img', 'image_index'])
    # print(df_train_with_imgs.shape)
    # print(len(images_train), len(files_train), len(files_train_name))
    df_train_with_imgs.image_index = df_train_with_imgs.image_index.astype(int)
    df_train.image_index = df_train.image_index.astype(int)
    df_train_with_imgs = df_train_with_imgs.merge(df_train, on='image_index', how='left')

    df_test_with_imgs = pd.DataFrame(data=list(zip(images_test, files_test, files_test_name)),
                                    columns=['img', 'path2img', 'image_index'])
    df_test_with_imgs.image_index = df_test_with_imgs.image_index.astype(int)
    df_test.image_index = df_test.image_index.astype(int)
    df_test_with_imgs = df_test_with_imgs.merge(df_test, on='image_index', how='left')

    print('---- LOAD IMGS ----')
    print('-- IMGS TRAIN --')
    print(len(images_train))
    print('-- IMGS TEST -- ')
    print(len(images_test))
    print('-- DF TRAIN --')
    print(df_test_with_imgs.shape)
    print('TRAIN IS NA ?')
    print(df_train_with_imgs.isna().any())
    print('TRAIN IS NULL ?')
    print(df_train_with_imgs.isnull().any())
    print('-- DF TEST --')
    print(df_test_with_imgs.shape)
    print('TEST IS NA ?')
    print(df_test_with_imgs.isna().any())
    print('TEST IS NULL ?')
    print(df_test_with_imgs.isnull().any())

    return images, images_train, images_test, files_train, files_test, df_train_with_imgs, df_test_with_imgs


# METRICS
# used for evaluation
#   Cumulative Matching Characteristic (CMC) curve
#   mean average precision (mAP)
# used for attr classification
#   classification accuracy
#   report the averaged accuracy of all these attribute predictions as the overall attribute prediction accuracy
# The gallery images are used as the testing set
# On Market-1501, we obtain rank-1 =87.04%, mAP = 66.89% by APR using the ResNet-50 model.

down_colors = slice(5, 14) # couleur du bas mutuellement exclusive
up_colors = slice(19, 27) # meme que couleur bas
binary_1 = slice(0, 5) # variables binaires
binary_2 = slice(14, 19)

def attr_accuracy(y_true, y_pred):
    # binary categories
    accuracy = binary_accuracy(y_true[:, binary_1], y_pred[:, binary_1]) * 0.5
    accuracy += binary_accuracy(y_true[:, binary_2], y_pred[:, binary_2]) * 0.5
    # top colors
    acc_top_color = categorical_accuracy(y_true[:, up_colors], y_pred[:, up_colors])
    # down colors
    acc_down_color = categorical_accuracy(y_true[:, down_colors], y_pred[:, down_colors])

    # print(acc, acc_down_color, acc_top_color)
    return accuracy * 9/11 + acc_down_color * 1/11 + acc_top_color * 1/11


def euclidian_dist(a: np.ndarray, b: float) -> np.ndarray:
    # Euclidian distance
    return np.linalg.norm(b - a, axis=-1)

def cmc_accuracy(features, ids, k=5, dist=euclidian_dist):
    res = 0
    for i, f in enumerate(features):
        n_ids = len(ids[ids[i] == ids]) - 1
        dists = dist(features, f)
        topk = ids[dists.argsort()][1: k + n_ids]
        res += len(topk[topk == ids[i]]) / n_ids
    return res / len(features)
