import json
import cv2
import os
import numpy as np
import pandas as pd
from subprocess import call
import matplotlib.pyplot as plt
from keras.applications.resnet50 import ResNet50, preprocess_input
from typing import Tuple
from keras import Input, Model
from keras.applications.resnet50 import ResNet50, preprocess_input
from keras.layers import Dense, Lambda, MaxPooling2D, Flatten, Concatenate, BatchNormalization, Dropout, Multiply
from IPython.display import Image
from keras.metrics import sparse_categorical_accuracy, binary_accuracy, categorical_accuracy
from keras.optimizers import SGD
from keras.callbacks import ModelCheckpoint, EarlyStopping, LearningRateScheduler
from utils import pre_process_y, REID_models, load_imgs, attr_accuracy


df_train, df_test, img_id_labels = pre_process_y()

images, images_train, images_test, files_train, files_test, df_train_with_imgs, df_test_with_imgs \
        = load_imgs(df_test, df_train)

_, id_model, training_model = REID_models(n_ids=1501)


_lambda = 0.9
optim = SGD()
# We choose SGD to converge faster and use the loss we defined on the utils file.
training_model.compile(optimizer=optim, loss=["binary_crossentropy", "sparse_categorical_crossentropy"],
                        metrics={"attributes":[attr_accuracy], "ids":[sparse_categorical_accuracy]},
                        loss_weights=[1 - _lambda, _lambda])

sample = slice(0, 2)
# n_samples = 400
# set the number of epochs to 60. The batch size is set to 32, it will be passed as parameter for the training.
epochs = 60
batch_size = 32

# We initialize the learning rate to 0.01 and then reduce to 0.001 from epoch 51
# We set a patience of 4 to stop our model if it stop improving
cb = [ EarlyStopping(patience=4, monitor = 'val_loss', ),
       ModelCheckpoint(f"weights/model-weights-{{epoch:02d}}.h5", save_weights_only=True),
       LearningRateScheduler(lambda epoch:0.01 if epoch < 50 else 0.001)
]

history = training_model.fit(
    x=preprocess_input(np.array(images_train[:])),
    y=[np.array(df_train_with_imgs[['age','backpack', 'bag', 'handbag', 'downblack', 'downblue',
        'downbrown', 'downgray', 'downgreen', 'downpink', 'downpurple',
        'downwhite', 'downyellow', 'upblack', 'upblue', 'upgreen', 'upgray',
        'uppurple', 'upred', 'upwhite', 'upyellow', 'clothes', 'down', 'up',
        'hair', 'hat', 'gender']].values[:]),
        np.array(df_train_with_imgs.image_index[:])],
    batch_size=batch_size,
    callbacks=cb,
    epochs=epochs,
    validation_split=0.2
)
print(history.history)

attributes, categorical_id = training_model.predict(preprocess_input(np.array(images_test)))
print(attributes.shape, categorical_id.shape)
print('attributes')
print(attributes)
print('categorical_id')
print(categorical_id)

x = images_train
file_names = files_train
# test function with query - gallery
test_data = np.array([
    x[0], x[1], x[2], x[100], x[200]
])
res = id_model.predict(preprocess_input(test_data))

# 0 1 2 are the same person, 3, 4 are different persons
dist = [
    np.linalg.norm(res[0] - res[1]),
    np.linalg.norm(res[0] - res[2]),
    np.linalg.norm(res[0] - res[3]),
    np.linalg.norm(res[0] - res[4]),
]
print(dist)

# We grab unseen data to test our model
test_data = np.array([
    x[400], x[401], x[402], x[500], x[600]
])
res = id_model.predict(preprocess_input(test_data))
# 0 1 2 are the same person, 3, 4 are different persons
dist = [
    # Distance Euclidienne
    np.linalg.norm(res[0] - res[1]),
    np.linalg.norm(res[0] - res[2]),
    np.linalg.norm(res[0] - res[3]),
    np.linalg.norm(res[0] - res[4]),
]
dist, (file_names[400], file_names[401], file_names[402], file_names[500], file_names[600])

#training_model.save_weights("../models/full-model-weights.h5")
