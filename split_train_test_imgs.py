import json
import cv2
import os
import numpy as np
import pandas as pd

with open('market_attribute.json') as file:
    data = json.load(file)

train = data['train']
test = data['test']

df_train = pd.DataFrame(
    data=list(zip(train['age'], train['backpack'], train['bag'], train['handbag'], train['downblack'], train['downblue'],
             train['downbrown'], train['downgray'], train['downgreen'], train['downpink'], train['downpurple'],
             train['downwhite'], train['downyellow'], train['upblack'], train['upblue'], train['upgreen'], train['upgray'],
             train['uppurple'], train['upred'], train['upwhite'], train['upyellow'], train['clothes'], train['down'],
             train['up'], train['hair'], train['hat'], train['gender'], train['image_index'])),
    columns=train.keys())

df_test = pd.DataFrame(
    data=list(zip(test['age'], test['backpack'], test['bag'], test['handbag'], test['downblack'], test['downblue'],
             test['downbrown'], test['downgray'], test['downgreen'], test['downpink'], test['downpurple'],
             test['downwhite'], test['downyellow'], test['upblack'], test['upblue'], test['upgreen'], test['upgray'],
             test['uppurple'], test['upred'], test['upwhite'], test['upyellow'], test['clothes'], test['down'],
             test['up'], test['hair'], test['hat'], test['gender'], test['image_index'])),
    columns=test.keys())

df_train.append(df_test)
print('df test')
df_test.info()
print('\ndf train')
df_train.info()


df_train.image_index

import cv2
from IPython.display import Image
import os
from subprocess import call
import pathlib

# import cv2
# from IPython.display import Image
# import os
# from subprocess import call
#
# images_filenames = os.listdir('Market-1501')
# images = []
# images_train = []
# images_test = []

# creer fichier par id
# files_test, files_train = [], []
# for file in images_filenames:
#     impath = os.path.join('Market-1501', file)
#     img = cv2.imread(impath)
#     images.append(img)
#     # Separation des imgs en train folder et test folder for DataImageGenerator(flow from directory)
#     # And separation des imgs in id folders
#     if file[0:4] in list(df_train.image_index):
#         images_train.append(file)
#         if file[0:4] not in files_train:
#             files_train.append(file[0:4])
#     elif file[0:4] in list(df_test.image_index):
#         images_test.append(file)
#         if file[0:4] not in files_test:
#             files_test.append(file[0:4])
#
# files_test.sort()
# files_train.sort()
#for elem_test, elem_train in zip(files_test, files_train):
    #print('mkdir test/{}'.format(elem_test))
    #print('mkdir train/{}'.format(elem_train))

# for file in images_train:
#     print('mv {0} train/{1}'.format(file,file[0:4]))
# for file in images_test:
#     print('mv {0} test/{1}'.format(file,file[0:4]))
# print(len(images_test))
# print(len(images_train))
# def split():
#     for elem in images_train:
#         call(['mkdir',  'Market-1501/test'])
#         path = os.path.join(pathlib.Path().absolute(), 'Market-1501')
#         yield call(['cd', path])
#         path = os.path.join(pathlib.Path().absolute(), 'Market-1501/train')
#         yield call(['mv', elem, path])
#     for elem in images_test:
#         call(['mkdir',  'Market-1501/train'])
#         path = os.path.join(pathlib.Path().absolute(), 'Market-1501')
#         yield call(['cd', 'Market-1501/train'])
#         path = os.path.join(pathlib.Path().absolute(), 'Market-1501/test')
#         yield call(['mv', elem, path])
# split()

# from keras.applications.resnet50 import ResNet50
# from keras.preprocessing import image
# from keras.models import Sequential
# from keras.layers import Dense, GlobalAveragePooling2D, Dropout, BatchNormalization
# from keras.applications.resnet50 import preprocess_input, decode_predictions
# import numpy as np

# model = Sequential()
# model.add(ResNet50(weights='imagenet', include_top=False, input_shape=(64, 128, 3)))
# model.add(GlobalAveragePooling2D())
# model.add(Dropout(0.5))
# model.add(Dense(512, activation='relu'))
# model.add(BatchNormalization())
# model.add(Dropout(0.5))
# model.add(Dense(27, activation='sigmoid', name='attributes'))

# model.compile(loss='cosine_proximity',
#               optimizer='adadelta',
#               metrics=['accuracy'])

from typing import Tuple
from keras import Input, Model
from keras.applications.resnet50 import ResNet50, preprocess_input
from keras.layers import Dense, Lambda, MaxPooling2D, Flatten, Concatenate, BatchNormalization, Dropout,Multiply

def get_models(n_person: int=751, n_attributes: int=27): # -> Tuple[Model, Model, Model]:
    input_tensor = Input(shape=(64, 128, 3))  # channels_last

    base_model = ResNet50(input_shape=(64,128, 3), pooling="avg", weights='imagenet',include_top=False)(input_tensor)

    attribute_layer = Dense(n_attributes, activation="sigmoid", name="attributes")(base_model)
    attributes_weight = Dense(n_attributes, activation="sigmoid")(attribute_layer)
    reweighted_attribute = Multiply()([attribute_layer, attributes_weight])

    id_layer = Dense(512, activation="relu")(base_model)
    id_layer = BatchNormalization()(id_layer)
    id_layer = Dropout(rate=0.5)(id_layer)
    id_layer = Concatenate(axis=-1, name="image_features")([reweighted_attribute, id_layer])

    training_id_layer = Dense(n_person, activation="softmax", name="ids")(id_layer)

    attribute_model = Model(input_tensor, attribute_layer)
    person_id_model = Model(input_tensor, id_layer)
    training_model = Model(input_tensor, [attribute_layer, training_id_layer])
    # training_model = Model(input_tensor, training_id_layer)
    return (attribute_model, training_model, person_id_model)
    # return training_model

# model_attribute,training_model,person_id_model = get_models()
_, training_model, _ = get_models()
training_model.summary()
# training_model.compile(loss='categorical_crossentropy',
#               optimizer='adam',
#               metrics=['accuracy'])
# optim = SGD()
from keras.metrics import sparse_categorical_accuracy
training_model.compile("adam", loss=["binary_crossentropy", "sparse_categorical_crossentropy"], 
                        metrics={"attributes":[binary_accuracy], "ids":[sparse_categorical_accuracy]})
from keras.preprocessing.image import ImageDataGenerator

# augumentations for training set:
train_datagen = ImageDataGenerator(rotation_range=20,
                                   rescale=1./255,
                                   width_shift_range=0.2,
                                   height_shift_range=0.2,
                                   shear_range=0.2,
                                   zoom_range=0.2,
                                   horizontal_flip=True,
                                   fill_mode='nearest',
                                   preprocessing_function=preprocess_input)
#rescale = set pixel value range 1-255
# shear_range = imagine feuille tourner (transvection in wiki for ex)
# only rescaling the validation set

# preprocessing_function is applied on each image but only after re-sizing & augmentation (resize => augment => pre-process)
# Each of the keras.application.resnet* preprocess_input MOSTLY mean BATCH NORMALIZATION (applied on each batch) stabilize the inputs to nonlinear activation functions
# Batch Normalization helps in faster convergence
test_datagen = ImageDataGenerator(rescale=1./255, preprocessing_function=preprocess_input)
# avoir tout sur la meme echelle

#train_generator = train_datagen.flow_from_dataframe(
   # dataframe=train_split,
 #   directory=celeba.images_folder,
 #   x_col='image_id',
   #  y_col=celeba.features_name,
    # target_size=(224, 224),
    #batch_size=batch_size,
    #class_mode='other'
#)

# data generators: creation de nouvelles images
train_generator = train_datagen.flow_from_directory(
        'Market-1501/train',
        target_size=(64, 128),
        batch_size=32,
        class_mode='binary')

test_generator = test_datagen.flow_from_directory(
        'Market-1501/test',
        target_size=(64, 128),
        batch_size=32,
        class_mode='binary')

# Len of generators = steps_per_epoch = nb of imgs / batch_size
# print(len(train_generator))
# print(len(test_generator))

training_model.fit_generator(
        train_generator,
        steps_per_epoch=393,
        epochs=60,
        validation_data=test_generator,
        validation_steps=397)