import json
import cv2
import os
import numpy as np
import pandas as pd
from subprocess import call
import matplotlib.pyplot as plt
from keras.applications.resnet50 import ResNet50, preprocess_input
from typing import Tuple
from keras import Input, Model
from keras.applications.resnet50 import ResNet50, preprocess_input
from keras.layers import Dense, Lambda, MaxPooling2D, Flatten, Concatenate, BatchNormalization, Dropout, Multiply
from IPython.display import Image
from keras.metrics import sparse_categorical_accuracy, binary_accuracy, categorical_accuracy
from keras.optimizers import SGD
from keras.callbacks import ModelCheckpoint, EarlyStopping, LearningRateScheduler
from utils import pre_process_y, REID_models, load_imgs, attr_accuracy

# for file in images_filenames:
#     impath = os.path.join('Market-1501/', file)
#     img = cv2.imread(impath)
#     images.append(img)
#     # Separation des imgs en train folder et test folder for DataImageGenerator(flow from directory)
#     # And separation des imgs in id folders

#     if file[0:4] in list(df_train.image_index):
#         images_train.append(img)
#         if file[0:4] not in files_train:
#             files_train.append(file[0:4])
#     elif file[0:4] in list(df_test.image_index):
#         images_test.append(file)
#         if file[0:4] not in files_test:
#             files_test.append(img)

model, _, _ = REID_models(n_attributes=27)
model.compile("adam", "binary_crossentropy", metrics=[attr_accuracy])
# cb = [
#     keras.callbacks.ModelCheckpoint(model_path + "market_attr{val_market_attr_accuracy:.2f}.h5", "val_acc"),
# ]
batch_size = 32
epochs = 5
sample = slice(0, 200)

df_train, df_test, img_id_labels = pre_process_y()

images, images_train, images_test, files_train, files_test, df_train_with_imgs, df_test_with_imgs \
        = load_imgs(df_test, df_train)

df_train = df_train.drop(['image_index'], axis=1)
df_test = df_test.drop(['image_index'], axis=1)
print(df_train.columns)
print(df_test.columns)

history = model.fit(preprocess_input(np.array(images_train[sample])), df_train.values[0:200],
                                     batch_size, epochs, validation_split=0.2)

plt.plot(range(epochs), history.history["loss"])
plt.plot(range(epochs), history.history["val_loss"])
plt.plot(range(epochs), history.history["val_attr_accuracy"])

res = model.predict(preprocess_input(np.array(images_train[sample])))
print([int(x) for x in np.round(res)[0]])
print(list(df_train.values[0]))
# * to print obj at @ (not <zip object at 0x7efddbdb2c30>)
# sep to add a \n at the end of each elem of the zip created list
print(*zip(np.round(res)[1], list(df_train.values[1]), df_train.columns), sep="\n")

print(model.evaluate(preprocess_input(np.array(images_test[sample])), df_test.values[0:200], batch_size=1, verbose=0))
